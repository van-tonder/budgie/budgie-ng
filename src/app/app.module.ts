import { NgModule }                      from '@angular/core';
import { BrowserModule }                 from '@angular/platform-browser';
import { BrowserAnimationsModule }       from '@angular/platform-browser/animations';

import { NbEvaIconsModule }              from '@nebular/eva-icons';
import { NbLayoutModule, NbThemeModule } from '@nebular/theme';

import { AppRoutingModule }              from 'src/app/app-routing.module';
import { AppComponent }                  from 'src/app/app.component';
import { environment }                   from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    environment.extModules
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
