import { MockModule } from 'src/app/ext/mock/mock.module';

// Extension modules for default development environments

export const DEV_EXT_MODULES = [
  MockModule
];
