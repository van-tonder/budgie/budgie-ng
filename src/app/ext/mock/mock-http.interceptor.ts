import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable }                                                         from '@angular/core';

import { Observable, of }                                                     from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MockHttpInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.fallback(request);
  }

  private fallback(request: HttpRequest<any>): Observable<HttpEvent<any>> {
    return of(new HttpResponse({ status: 200 }));
  }
}

const http = {
  get: (request: HttpRequest<any>) => ({
    to: (endpoint: string) => request.method === 'GET' && request.url.endsWith(endpoint)
  }),
  put: (request: HttpRequest<any>) => ({
    to: (endpoint: string) => request.method === 'PUT' && request.url.endsWith(endpoint)
  }),
  post: (request: HttpRequest<any>) => ({
    to: (endpoint: string) => request.method === 'POST' && request.url.endsWith(endpoint)
  }),
  delete: (request: HttpRequest<any>) => ({
    to: (endpoint: string) => request.method === 'DELETE' && request.url.endsWith(endpoint)
  }),
};
