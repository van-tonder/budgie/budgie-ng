import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf } from '@angular/core';

import { ShouldOnlyImportOnceModule }   from 'src/app/shared/guard/should-only-import-once-module.abstract';

@NgModule({
  imports: [
    CommonModule
  ]
})
export class CoreModule extends ShouldOnlyImportOnceModule {

  constructor(@SkipSelf() @Optional() parent: CoreModule) {
    super(parent);
  }
 }
