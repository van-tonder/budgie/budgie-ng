import { PROD_EXT_MODULES } from 'src/app/ext/index.prod';

export const environment = {
  production: true,
  extModules: PROD_EXT_MODULES
};
